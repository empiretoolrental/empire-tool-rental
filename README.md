Empire Tool Rental is a construction equipment and tool rental/sales/service/repair company located in the Bronx, New York serving the tri-state area.  Empire Tool rents, sells, and services tools and equipment for contractors, municipalities, special events, facility managers, and more.

Address: 796 East 140th Street, Bronx, NY 10454, USA

Phone: 718-939-9300

Website: https://empiretoolrental.com
